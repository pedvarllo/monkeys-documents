[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](http://perso.crans.org/besson/LICENSE.html)
# MONKEYS-DOCUMENTS #

## Project Description ##

This is the monkeys-documents microservice which belongs to the monkeys-tousled application.
It manages the documents of the application who can be only created for registered users.

## Getting started ##

A makefile has been given in order to run the application. Running the application is
very easy. All you need to do is run the following command in the root path of the project:

```make run-app```

Make sure you previously run the discovery and the api-gateway-service microservices.
For further information, you can read the README.md of these projects.


In order to run the application tests, you have to type in the console at the
root path of the microservice the following command:

``make test-all``

## How to use this project ##

Once the microservice is up, you can find a description of the API here: []


## Dependencies ##

* Spring framework:
    * Spring JPA
    * Spring Web
    * Spring Cloud
    * Spring Security
    * Spring Oauth2 Resource Server
    * Spring Kafka
* Liquibase
* Keycloak
* Postgresql
* Lombock
* Nimbusds
* Maven

### Authors ###

* **Pedro Varela Llorente**
    * [email](pedvarllo@gmail.com)
    * [LinkedIn](https://www.linkedin.com/in/pedvarllo/)
