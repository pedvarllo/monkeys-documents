package com.tousled.monkeys.repositories;


import com.tousled.monkeys.models.Document;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface DocumentRepository extends JpaRepository<Document, UUID> {
}
