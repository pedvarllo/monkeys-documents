package com.tousled.monkeys.controllers;

import com.tousled.monkeys.models.dto.DocumentCreatedResponse;
import com.tousled.monkeys.services.DocumentRegistrationService;
import liquibase.pro.packaged.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/documents")
public class DocumentsCommandController {

    private final DocumentRegistrationService documentRegistrationService;

    @Autowired
    public DocumentsCommandController(DocumentRegistrationService documentRegistrationService) {
        this.documentRegistrationService = documentRegistrationService;
    }


    @PostMapping(consumes = { MediaType.MULTIPART_FORM_DATA_VALUE })
    public ResponseEntity<DocumentCreatedResponse> postDocument(@RequestPart("file") MultipartFile file,
                                                                @RequestParam("ownerId") UUID ownerId,
                                                                @RequestHeader("x-auth-user-id") String userId) throws IOException {

        String documentName = this.documentRegistrationService.uploadDocument(file, ownerId, userId);
        DocumentCreatedResponse response = DocumentCreatedResponse.builder().documentName(documentName).build();

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}
