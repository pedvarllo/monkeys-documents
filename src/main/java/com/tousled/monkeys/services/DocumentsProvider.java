package com.tousled.monkeys.services;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface DocumentsProvider {
    void uploadFile(MultipartFile file) throws IOException;
}
