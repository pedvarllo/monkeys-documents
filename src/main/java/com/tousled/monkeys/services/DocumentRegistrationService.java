package com.tousled.monkeys.services;

import com.tousled.monkeys.events.EventProducer;
import com.tousled.monkeys.events.dto.DocumentEventPayload;
import com.tousled.monkeys.models.Document;
import com.tousled.monkeys.repositories.DocumentRepository;
import com.tousled.theagilemonkeys.exceptions.IntegrationException;
import com.tousled.theagilemonkeys.exceptions.NotFoundException;
import org.apache.commons.io.FilenameUtils;
import org.aspectj.runtime.internal.Conversions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service
public class DocumentRegistrationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentRegistrationService.class);
    private static final String DOCUMENT_NOT_UPLOADED_PROVIDER_ERROR_MESSAGE = "The document with name %s could not be uploaded";

    private final String commonPath;
    private final DocumentsProvider documentsProvider;
    private final DocumentRepository documentRepository;
    private final ConversionService conversionService;
    private final EventProducer eventProducer;

    @Autowired
    public DocumentRegistrationService(@Value("${monkeys-documents.common-path}") String commonPath,
                                       DocumentsProvider documentsProvider,
                                       DocumentRepository documentRepository,
                                       ConversionService conversionService,
                                       EventProducer eventProducer) {
        this.commonPath = commonPath;
        this.documentsProvider = documentsProvider;
        this.documentRepository = documentRepository;
        this.conversionService = conversionService;
        this.eventProducer = eventProducer;
    }

    public String uploadDocument(MultipartFile multipartFile, UUID ownerId, String userId) {
        LOGGER.info("Retrieving extension of file");
        String extension = FilenameUtils.getExtension(multipartFile.getOriginalFilename());

        LOGGER.info("Retrieving path for saving the file");
        String pathName = commonPath + "/" + UUID.randomUUID() + "." + extension;

        if (saveDocumentInProvider(multipartFile)) {
            Document document = Document.builder()
                    .ownerId(ownerId)
                    .path(pathName)
                    .userId(userId)
                    .build();

            LOGGER.info("Saving document in our database with id {} ", document.getId());
            documentRepository.save(document);

            DocumentEventPayload event = this.conversionService.convert(document, DocumentEventPayload.class);
            LOGGER.info("Producing create document event");
            eventProducer.sendEvent(event);
        }
        return multipartFile.getOriginalFilename();
    }

    private boolean saveDocumentInProvider(MultipartFile multipartFile) {
        try {
            this.documentsProvider.uploadFile(multipartFile);
        } catch (IOException e) {
            throw new IntegrationException(String.format(DOCUMENT_NOT_UPLOADED_PROVIDER_ERROR_MESSAGE, multipartFile.getOriginalFilename()));
        }
        return true;
    }


}
