package com.tousled.monkeys.clients;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.tousled.monkeys.services.DocumentsProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

@Component
public class CloudinaryDocumentsAdapter implements DocumentsProvider {

    private final Cloudinary cloudinaryConfig;

    @Autowired
    public CloudinaryDocumentsAdapter(Cloudinary cloudinary) {
        this.cloudinaryConfig = cloudinary;
    }

    @Override
    public void uploadFile(MultipartFile multipartFile) throws IOException {
        File file = convertMultiPartToFile(multipartFile);
        this.cloudinaryConfig.uploader().upload(file, ObjectUtils.emptyMap());
    }

    private File convertMultiPartToFile(MultipartFile multipartFile) throws IOException {
        File convFile = new File(Objects.requireNonNull(multipartFile.getOriginalFilename()));
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(multipartFile.getBytes());
        fos.close();
        return convFile;
    }
}
