package com.tousled.monkeys.models;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import java.util.UUID;

@Data
@Builder
@Entity
@Table(name = "documents", schema = "monkeys_documents")
public class Document {

    @Id
    @GeneratedValue
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column(name = "owner_id", nullable = false)
    private UUID ownerId;

    @Column(name = "user_id", nullable = false)
    private String userId;

    @Column(name = "path", nullable = false)
    private String path;

    @Tolerate
    public Document() {
    }
}
