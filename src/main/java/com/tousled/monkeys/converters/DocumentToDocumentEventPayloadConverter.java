package com.tousled.monkeys.converters;

import com.tousled.monkeys.events.dto.DocumentEventPayload;
import com.tousled.monkeys.models.Document;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class DocumentToDocumentEventPayloadConverter implements Converter<Document, DocumentEventPayload> {

    @Override
    public DocumentEventPayload convert(Document source) {
        return DocumentEventPayload.builder()
                .id(source.getId())
                .documentPath(source.getPath())
                .ownerId(source.getOwnerId())
                .userId(source.getUserId())
                .build();
    }
}
