package com.tousled.monkeys.events;

import com.tousled.monkeys.events.dto.DocumentEventPayload;

public interface EventProducer {

    void sendEvent(DocumentEventPayload customer);
}
