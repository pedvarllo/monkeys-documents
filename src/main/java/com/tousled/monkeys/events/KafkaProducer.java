package com.tousled.monkeys.events;

import com.tousled.monkeys.events.dto.DocumentEventPayload;
import com.tousled.monkeys.models.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class KafkaProducer implements EventProducer {

    private final KafkaTemplate<UUID, DocumentEventPayload> kafkaTemplate;
    private final String topic;

    @Autowired
    public KafkaProducer(KafkaTemplate<UUID, DocumentEventPayload> kafkaTemplate,
                         @Value("${kafka.topic.name}") String topic) {
        this.kafkaTemplate = kafkaTemplate;
        this.topic = topic;
    }

    @Override
    public void sendEvent(DocumentEventPayload event) {
        this.kafkaTemplate.send(topic, event);
    }
}
