package com.tousled.monkeys.events.dto;

import lombok.*;
import lombok.experimental.Tolerate;

import java.util.UUID;

@Data
@AllArgsConstructor
@Builder
@ToString
public class OwnerEventPayload {

    private UUID id;
    private String name;
    private String surname;

    @Tolerate
    public OwnerEventPayload() {
    }
}
