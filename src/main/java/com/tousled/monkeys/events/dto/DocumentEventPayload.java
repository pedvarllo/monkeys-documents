package com.tousled.monkeys.events.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.UUID;

@Data
@AllArgsConstructor
@Builder
@ToString
public class DocumentEventPayload {

    private UUID id;
    private UUID ownerId;
    private String documentPath;
    private String userId;
}
