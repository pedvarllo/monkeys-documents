package com.tousled.monkeys.events;

import com.tousled.monkeys.events.dto.OwnerEventPayload;
import com.tousled.monkeys.services.DocumentRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    private final DocumentRegistrationService documentRegistrationService;

    @Autowired
    public KafkaConsumer(DocumentRegistrationService documentRegistrationService) {
        this.documentRegistrationService = documentRegistrationService;
    }

    @KafkaListener(topics = "${kafka.customers.topic.name}", groupId = "${spring.kafka.consumer.group-id}", containerFactory = "customersListener")
    public void consumeEvents(OwnerEventPayload ownerEventPayload) {
        System.out.println("consuming events");
    }
}
