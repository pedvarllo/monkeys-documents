package com.tousled.monkeys;

import com.cloudinary.Cloudinary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import java.time.Clock;
import java.util.Map;

@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
public class MonkeysDocumentsApplication {

    private static final String CLOUD_NAME_PROPERTY = "cloud_name";
    private static final String API_KEY_PROPERTY = "api_key";
    private static final String API_SECRET_PROPERTY = "api_secret";
    private final String cloudName;
    private final String apiKey;
    private final String apiSecret;

    @Autowired
    public MonkeysDocumentsApplication(@Value("${cloudinary.cloud-name}") String cloudName,
                                       @Value("${cloudinary.api-key}") String apiKey,
                                       @Value("${cloudinary.api-secret}") String apiSecret) {
        this.cloudName = cloudName;
        this.apiKey = apiKey;
        this.apiSecret = apiSecret;
    }

    public static void main(String[] args) {
        SpringApplication.run(MonkeysDocumentsApplication.class, args);
    }

    @Bean
    public Cloudinary cloudinaryConfig() {
        Map<String, String> config = Map.of(CLOUD_NAME_PROPERTY, cloudName,
                API_KEY_PROPERTY, apiKey,
                API_SECRET_PROPERTY, apiSecret);
        return new Cloudinary(config);
    }
}
