package com.tousled.monkeys.services;

import com.tousled.monkeys.events.EventProducer;
import com.tousled.monkeys.events.dto.DocumentEventPayload;
import com.tousled.monkeys.models.Document;
import com.tousled.monkeys.repositories.DocumentRepository;
import com.tousled.theagilemonkeys.exceptions.IntegrationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class DocumentRegistrationServiceTest {

    private static final String DOCUMENT_NOT_UPLOADED_PROVIDER_ERROR_MESSAGE = "The document with name %s could not be uploaded";

    private String commonPath;
    private DocumentsProvider documentsProvider;
    private DocumentRepository documentRepository;
    private ConversionService conversionService;
    private EventProducer eventProducer;
    private DocumentRegistrationService sut;

    @BeforeEach
    void setUp() {
        this.commonPath = "path";
        this.documentsProvider = mock(DocumentsProvider.class);
        this.documentRepository = mock(DocumentRepository.class);
        this.conversionService = mock(ConversionService.class);
        this.eventProducer = mock(EventProducer.class);
        this.sut = new DocumentRegistrationService(commonPath, documentsProvider, documentRepository, conversionService, eventProducer);
    }

    @Test
    void itShouldUploadADocument() throws IOException {

        UUID ownerId = UUID.randomUUID();
        MultipartFile multipartFile = mock(MultipartFile.class);
        DocumentEventPayload documentEventPayload = mock(DocumentEventPayload.class);
        String userId = "x-auth-user-id";

        doNothing().when(multipartFile).transferTo(any(File.class));
        when(conversionService.convert(any(Document.class), eq(DocumentEventPayload.class))).thenReturn(documentEventPayload);
        when(multipartFile.getOriginalFilename()).thenReturn("document.png");

        String actualResponse = this.sut.uploadDocument(multipartFile, ownerId, userId);

        // Difficult to test because static methods random uuid
        verify(documentRepository).save(any());
        verify(eventProducer).sendEvent(any());
    }

    @Test
    void itShouldNotSaveDocumentIfItCouldNotBeUpload() throws IOException {

        UUID ownerId = UUID.randomUUID();
        MultipartFile multipartFile = mock(MultipartFile.class);
        String userId = "x-auth-user-id";
        String fileName = "document.png";

        when(multipartFile.getOriginalFilename()).thenReturn(fileName);
        doThrow(IOException.class).when(documentsProvider).uploadFile(multipartFile);

        Exception actualException = assertThrows(Exception.class, () -> this.sut.uploadDocument(multipartFile, ownerId, userId));

        verifyNoInteractions(conversionService);
        verifyNoInteractions(documentRepository);
        verifyNoInteractions(eventProducer);

        assertEquals(IntegrationException.class, actualException.getClass());
        assertEquals(String.format(DOCUMENT_NOT_UPLOADED_PROVIDER_ERROR_MESSAGE, fileName), actualException.getMessage());
    }


}