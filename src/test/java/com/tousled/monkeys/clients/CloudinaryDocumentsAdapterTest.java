package com.tousled.monkeys.clients;

import com.cloudinary.Cloudinary;
import com.cloudinary.Uploader;
import com.cloudinary.utils.ObjectUtils;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class CloudinaryDocumentsAdapterTest {

    private Cloudinary cloudinaryConfig;
    private CloudinaryDocumentsAdapter cut;

    @BeforeEach
    void setUp() {
        this.cloudinaryConfig = mock(Cloudinary.class);
        this.cut = new CloudinaryDocumentsAdapter(cloudinaryConfig);
    }

    @Test
    void itShouldUploadDocuments() throws IOException {
        MultipartFile multipartFile = mock(MultipartFile.class);
        File file = new File("originalFileName");
        Uploader uploader = mock(Uploader.class);
        Map objectMap = ObjectUtils.emptyMap();
        Map result = ObjectUtils.emptyMap();

        when(multipartFile.getOriginalFilename()).thenReturn("originalFileName");
        when(multipartFile.getBytes()).thenReturn("content".getBytes());
        when(cloudinaryConfig.uploader()).thenReturn(uploader);
        when(uploader.upload(file, objectMap)).thenReturn(result);


        this.cut.uploadFile(multipartFile);

        assertEquals(FileUtils.readFileToString(file, StandardCharsets.UTF_8), "content");
        verify(cloudinaryConfig).uploader();
        verify(uploader).upload(file, objectMap);
    }
}