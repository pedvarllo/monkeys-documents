package com.tousled.monkeys.controllers;

import com.tousled.monkeys.models.dto.DocumentCreatedResponse;
import com.tousled.monkeys.services.DocumentRegistrationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class DocumentsCommandControllerTest {

    private DocumentRegistrationService documentRegistrationService;
    private DocumentsCommandController cut;

    @BeforeEach
    void setUp() {
        this.documentRegistrationService = mock(DocumentRegistrationService.class);
        this.cut = new DocumentsCommandController(documentRegistrationService);
    }

    @Test
    void itShouldPostDocuments() throws IOException {

        MultipartFile file = mock(MultipartFile.class);
        UUID ownerId = UUID.randomUUID();
        String userId = "x-auth-user-id";
        String documentName = "document.png";
        DocumentCreatedResponse expectedResponse = DocumentCreatedResponse.builder().documentName(documentName).build();

        when(documentRegistrationService.uploadDocument(file, ownerId, userId)).thenReturn(documentName);

        ResponseEntity<DocumentCreatedResponse> actualResponse = this.cut.postDocument(file, ownerId, userId);

        verify(documentRegistrationService).uploadDocument(eq(file), eq(ownerId), eq(userId));

        assertEquals(HttpStatus.CREATED, actualResponse.getStatusCode());
        assertEquals(expectedResponse, actualResponse.getBody());
    }
}