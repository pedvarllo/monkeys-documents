package com.tousled.monkeys;

import com.tousled.monkeys.models.Document;

import java.util.UUID;

public class DocumentObjectMotherBuilder {

    public static Document createDocument(String path) {
        return Document.builder()
                .id(UUID.randomUUID())
                .path(path)
                .ownerId(UUID.randomUUID())
                .userId(UUID.randomUUID().toString())
                .build();

    }
}
