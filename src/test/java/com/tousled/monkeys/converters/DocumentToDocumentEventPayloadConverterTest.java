package com.tousled.monkeys.converters;

import com.tousled.monkeys.DocumentObjectMotherBuilder;
import com.tousled.monkeys.events.dto.DocumentEventPayload;
import com.tousled.monkeys.models.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DocumentToDocumentEventPayloadConverterTest {

    private DocumentToDocumentEventPayloadConverter cut;


    @BeforeEach
    void setUp() {
        this.cut = new DocumentToDocumentEventPayloadConverter();
    }

    @Test
    void itShouldConvertFromDocumentToEvent() {

        Document document = DocumentObjectMotherBuilder.createDocument("path");

        DocumentEventPayload expectedEvent = this.cut.convert(document);

        assertEquals(document.getId(), expectedEvent.getId());
        assertEquals(document.getOwnerId(), expectedEvent.getOwnerId());
        assertEquals(document.getPath(), expectedEvent.getDocumentPath());
        assertEquals(document.getUserId(), expectedEvent.getUserId());
    }
}