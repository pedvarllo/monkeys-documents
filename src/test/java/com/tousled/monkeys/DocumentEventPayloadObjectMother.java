package com.tousled.monkeys;

import com.tousled.monkeys.events.dto.DocumentEventPayload;

import java.util.UUID;

public class DocumentEventPayloadObjectMother {

    public static DocumentEventPayload createDocumentEventPayload(UUID id, UUID ownerId, String path, String userId) {
        return DocumentEventPayload.builder()
                .id(id)
                .documentPath(path)
                .userId(userId)
                .ownerId(ownerId)
                .build();
    }
}
