package com.tousled.monkeys.events;

import com.tousled.monkeys.DocumentEventPayloadObjectMother;
import com.tousled.monkeys.DocumentObjectMotherBuilder;
import com.tousled.monkeys.events.dto.DocumentEventPayload;
import com.tousled.monkeys.models.Document;
import liquibase.pro.packaged.D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.kafka.core.KafkaTemplate;

import java.util.UUID;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class KafkaProducerTest {

    private KafkaTemplate<UUID, DocumentEventPayload> kafkaTemplate;
    private String topic;
    private KafkaProducer cut;

    @BeforeEach
    void setUp() {
        this.kafkaTemplate = mock(KafkaTemplate.class);
        this.topic = "documents_topic";
        this.cut = new KafkaProducer(kafkaTemplate, topic);
    }

    @Test
    void itShouldSendDocumentEventPayload() {
        DocumentEventPayload documentEventPayload = mock(DocumentEventPayload.class);

        this.cut.sendEvent(documentEventPayload);

        verify(kafkaTemplate).send(topic, documentEventPayload);
    }
}